/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Chuong trinh nhan thong tin sinh vien
 * @author Nguyen Duc
 * @version 1.8
 * @since 2018
 */
public class StudentManagement {
    //tao mot mang cac phan thu thuoc kieu Student, mang co toi da 100 phan tu
	static public Student[] svs = new Student[100];
}
class Student {
	private String name;
	private String id;
	private String group;
	private String email;
	private int marked;
/**
 * ham khoi tao sinh vien voi cac gia tri mac dinh
*/

public Student(String n, String sid, String gr, String em){
	this.name = n;
	this.id = sid;
	this.group = gr;
	this.email = em;
	this.marked = 0;
	}
/**
 * ham khoi tao sinh vien voi cac gia tri duoc nhap vao
 * @param Stirng name la bien dau tien. the hien ten cua sinh vien
 * @param String id la bien thu 2. the hien mssv cua sinh vien
 * @param String email la bien thu 3. the hien email cua sinh vien
*/
public Student(String n, String sid, String em){
	this.name = n;
	this.id = sid;
	this.group = "K62";
	this.email = em;
	}
/**
 * ham khoi tao sinh vien voi cac gia tri giong voi 1 sinh vien khac
 * @param Student la bien duy nhat. chua cac gia tri cua sinh vien khac
*/
public Student(Student s){
	this.name = s.name;
	this.id = s.id;
	this.group = s.group;
	this.email = s.email;
	}
/**
 * getName de lay ten cua sv
 * @return ham tra lai gia tri ten cua sv
*/
public String getName(){
	return this.name;
	}
/**
 * ham setName de dat ten cho sv
 * @param String n la ten cua sv
 * @return ham ko tra lai gia tri nao
*/
public void setName(String n){
	this.name = n;
	}
/**
 * getId de lay mssv cua sv
 * @return ham tra lai gia tri mssv cua sv
*/
public String getId(){
	return this.id;
	}
/**
 * ham setId de dat mssv cho sv
 * @param String n la mssv cua sv
 * @return ham ko tra lai gia tri nao
*/
public void setId(String n){
	this.id = n;
	}
/**
 * getGroup de lay lop cua sv
 * @return ham tra lai gia tri lop cua sv
*/
public String getGroup(){
	return this.group;
	}
/**
 * ham setGroup de dat lop cho sv
 * @param String n la lop cua sv
 * @return ham ko tra lai gia tri nao
*/
public void setGroup(String n){
	this.group = n;
	}
/**
 * getEmail de lay email cua sv
 * @return ham tra lai gia tri email cua sv
*/
public String getEmail(){
	return this.email;
	}
/**
 * ham setEmail de dat email cho sv
 * @param String n la email cua sv
 * @return ham ko tra lai gia tri nao
*/
public void setEmail(String n){
	this.email = n;
	}
/**
 * getInfo de lay thong tin cua sv
 * @return ham tra lai 1 chuoi gom ten, mssv, lop va email cua sv
*/
public String getInfo(){
	String info = name + " " + id + " " + group + " " + email;
	return info;
	}
/**
 * getMarked de kiem tra xem sv da duoc danh dau hay chua
 * @return tra lai trang thai cua sinh vien
*/
public int getMarked(){
	return this.marked;
	}
/**
 * setMakerd thay doi trang thai da duoc danh dau cua phan tu
 * @param int s la so trang thai nhap vao
*/
public void setMarked(int s){
	this.marked = s;
	}


}