


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Chuong trinh cho phep thuc hien cong tru nhan chia phan so
 * @author Nguyen Duc
 * @version 1.8
 * @since 2018
 */
public class PS {
    private int tu, mau;
 
    public PS(int tu, int mau) {
        
        this.tu = tu;
        this.mau = mau;
    }
     
     public int getTu() {
        return tu;
    }
 
    public void setTu(int tu) {
        this.tu = tu;
    }
 
    public int getMau() {
        return mau;
    }
 
    public void setMau(int mau) {
        this.mau = mau;
    }
    /**
     * Ham tim ucln a va b
     * @param a so nguyen duong
     * @param b so nguyen duong
     * @return ucln cua a va b
     */
    public int USCLN(int a, int b) {
        if (b == 0) return a;
        return USCLN(b, a % b);
      
    }
    /**
     * ham rut gon phan so
     * @param i ucln cua tu so va mau so
     */ 
    public void toiGianPhanSo() {
        int i = USCLN(this.getTu(), this.getMau());
        this.setTu(this.getTu() / i);
        this.setMau(this.getMau() / i);
    }
    
    /**
     * cong phan so
     * @param ts t] so cua ps tong
     * @param ms mau so cua ps tong
     */
    public void congPhanSo(PS ps) {
        int ts = this.getTu() * ps.getMau() + ps.getTu() * this.getMau();
        int ms = this.getMau() * ps.getMau();
        PS phanSoTong = new PS(ts, ms);
        phanSoTong.toiGianPhanSo();
        System.out.println("Tong = " + phanSoTong.tu + "/" + phanSoTong.mau);
    }
    /**
     * tru 2 phan so
     * @param ts tu so cua ps hieu
     * @param ms mau so cua ps hieu
     */
     public void truPhanSo(PS ps) {
        int ts = this.getTu() * ps.getMau() - ps.getTu() * this.getMau();
        int ms = this.getMau() * ps.getMau();
        PS phanSoHieu = new PS(ts, ms);
        phanSoHieu.toiGianPhanSo();
        System.out.println("Hieu = " + phanSoHieu.tu + "/" + phanSoHieu.mau);
    }
    /**
     * nhan 2 phan so
     * @param ts tu so cua ps tich
     * @param ms mau so cua ps tich
     */
     public void nhanPhanSo(PS ps) {
        int ts = this.getTu() * ps.getTu();
        int ms = this.getMau() * ps.getMau();
        PS phanSoTich = new PS(ts, ms);
        phanSoTich.toiGianPhanSo();
        System.out.println("Tich = " + phanSoTich.tu + "/" + phanSoTich.mau);
    }
    /**
     * Chia 2 phan so
     * @param ts tu so cua ps thuong
     * @param ms mau so cua ps thuong
     */
     public void chiaPhanSo(PS ps) {
        int ts = this.getTu() * ps.getMau();
        int ms = this.getMau() * ps.getTu();
        PS phanSoThuong = new PS(ts, ms);
        phanSoThuong.toiGianPhanSo();
        System.out.println("Thuong= " + phanSoThuong.tu + "/" + phanSoThuong.mau);
    }
    /**
     * So sanh 2 phan so
     * 
     */
     public boolean equals(PS ps){
         if((this.getTu() * ps.getMau()) > (this.getMau() * ps.getTu()) )
             return true;
         else return false;
     }
             
}