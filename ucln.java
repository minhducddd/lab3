/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/** 
 * Chuong trinh tim ucln cua 2 so nguyen a,b
 * @author Duc
 * @version 1.8
 * @since 2018
 */
public class ucln {
    /**
     * Tim uoc chung lon nhat (USCLN)
     * 
     * @param a: so nguyen duong
     * @param b: so nguyen duong
     * @return USCLN a va b
     */
    public static int USCLN(int a, int b) {
        a=Math.abs(a);b=Math.abs(b);
        if (b == 0) return a;
        return USCLN(b, a % b);
    }  
    /**
     * main
     * 
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(USCLN(9,3));
    }
}